# consul 文档

欢迎使用 Consul 文档！该文档是 Consul 的所有可用功能和选项的参考资料

在下面的快速链接中，您将找到最常用的文档和指向我们的指南的链接，这些指南将引导您完成常见任务。请注意，这些指南位于 HashiCorp Learn 网站上。

- 按照[文档](https://www.consul.io/docs/install)使用预编译的二进制文件或从源代码安装 Consul。
- 阅读更多关于 Consul 服务器和客户端的[配置选项](https://www.consul.io/docs/agent/options)。
- [通过HashiCorp Learn](https://learn.hashicorp.com/consul)的分步指南开始使用 Consul 。



## 一、什么是consul

### consul 简介

欢迎来到 Consul 的介绍指南！本指南是开始使用 Consul 的最佳场所。我们将介绍 Consul 是什么，它可以解决什么问题，它与现有软件的比较，以及如何开始使用它。如果您熟悉 Consul 的基础知识，该[文档](https://www.consul.io/docs)提供了更详细的可用功能参考。如果您准备好获得实践经验，请使用我们的 [HashiCorp 学习教程](https://learn.hashicorp.com/tutorials/consul/get-started-install)在本地部署 Consul 。

#### 1、为什么是consul

Consul 解决了各种规模的组织在微服务架构中遇到的挑战。这范围从在各种分布式环境和地理位置中运行，到满足保护所有应用程序流量的需要。世界正在迅速变化和发展，计算网络层也是如此。

当今的网络必须快速适应并确保始终加密通信。Consul 使组织能够在扩大规模的同时采用[零信任](https://www.hashicorp.com/solutions/zero-trust-security)模型。Consul 可以实现这一切，同时通过关键网络任务的自动化减轻运营商和开发人员的负担

![解释为什么领事的图表](https://mktg-content-api-hashicorp.vercel.app/api/assets?product=consul&version=refs%2Fheads%2Fstable-website&asset=website%2Fpublic%2F%2Fimg%2Fintro_why_consul_diagram.svg)

#### 2、什么是consul

Consul 是一个服务网格解决方案，提供具有服务发现、配置和分段功能的全功能控制层。这些功能中的每一个都可以根据需要单独使用，也可以一起使用来构建完整的服务网格。Consul 需要控制层并支持代理和原生集成模型。Consul 附带一个简单的内置代理，因此一切都可以开箱即用，而且还支持 Envoy 等 3rd 方代理集成。

Consul 的主要特点是：

- **服务发现**：Consul 的客户端可以注册服务，例如 `api`或 `mysql`，其他客户端可以使用 Consul 发现给定服务的提供者。使用 DNS 或 HTTP，应用程序可以轻松找到它们所依赖的服务。
- **健康检查**：Consul 客户端可以提供任意数量的健康检查，或者与给定服务相关联（“网络服务器是否返回 200 OK”），或者与本地节点相关联（“内存利用率是否低于 90%”）。操作员可以使用此信息来监控集群的健康状况，服务发现组件使用它来将流量从不健康的主机中路由出去。
- **KV 存储**：应用程序可以将 Consul 的分层key/value存储用于任意数量的目的，包括动态配置、特征标记、协调、领导者选举等。简单的 HTTP API 使其易于使用。
- **安全服务通信**：Consul 可以为服务生成和分发 TLS 证书以建立相互 TLS 连接。 [意图](https://www.consul.io/docs/connect/intentions) 可用于定义允许通信的服务。可以通过可以实时更改的意图轻松管理服务分段，而不是使用复杂的网络拓扑和静态防火墙规则。
- **多数据中心**：Consul 支持开箱即用的多个数据中心。这意味着 Consul 的用户不必担心构建额外的抽象层以扩展到多个区域。

Consul 旨在对 DevOps 社区和应用程序开发人员都友好，使其非常适合现代、弹性的基础设施。

#### 3、Consul的基本架构

Consul 是一个分布式的、高可用的系统。本节将介绍基础知识，故意省略一些不必要的细节，以便您快速了解 Consul 的工作原理。有关更多详细信息，请参阅 [深入的架构概述](https://www.consul.io/docs/architecture)。

每个为 Consul 提供服务的节点都运行一个*Consul 代理*。发现其他服务或获取/设置键/值数据不需要运行代理。代理负责节点上的服务以及节点本身的健康检查。

代理与一台或多台*Consul 服务器*交谈。Consul 服务器是存储和复制数据的地方。服务器自己选举领导者。虽然 Consul 可以与一台服务器一起运行，但建议使用 3 到 5 台，以避免导致数据丢失的故障场景。建议为每个数据中心使用一组 Consul 服务器。

服务器维护一个*目录*，该目录是通过聚合代理提交的信息形成的。该目录维护集群的高级视图，包括哪些服务可用、哪些节点运行这些服务、健康信息等等。可以在 [此处](https://www.consul.io/docs/architecture/anti-entropy#catalog)找到代理和目录如何交互。

需要发现其他服务或节点的基础架构组件可以查询任何 Consul 服务器*或*任何 Consul 代理。代理会自动将查询转发到服务器。

每个数据中心运行一个 Consul 服务器集群。当提出跨数据中心的服务发现或配置请求时，本地 Consul 服务器将请求转发到远程数据中心并返回结果。

### 用例

#### 1、什么是服务网格Service Mesh

*服务网格*是一个专用网络层，可在基础架构内和跨基础架构（包括本地和云环境）提供安全的服务到服务通信。服务网格通常与微服务架构模式一起使用，但可以在涉及复杂网络的任何场景中提供价值。

#### 2、服务网格的好处

服务网格为所有组织带来好处，从安全性到提高应用程序弹性。服务网格的一些好处包括：

- 服务发现
- 应用程序健康监控
- 负载均衡
- 自动故障转移
- 交通管理
- 加密
- 可观察性和可追溯性，
- 身份验证和授权，
- 网络自动化

利用服务网格的一个常见用例是实现[*零信任*模型](https://www.consul.io/use-cases/zero-trust-networking)。在零信任模型中，应用程序需要基于身份的访问，以确保服务网格内的所有通信都使用 TLS 证书进行身份验证并在传输过程中加密。

在传统的安全策略中，保护主要集中在网络的外围。在云环境中，网络访问的表面积比传统的本地网络要宽得多。此外，传统的安全实践忽略了许多不良行为者可能来自网络墙内的事实。零信任模型解决了这些问题，同时允许组织根据需要进行扩展。

#### 3、服务网格如何工作？

服务网格通常由控制层和数据层组成。控制层维护一个中央注册表，用于跟踪所有服务及其各自的 IP 地址。此活动称为[服务发现](https://www.hashicorp.com/products/consul/service-discovery-and-health-checking)。只要应用程序在控制层上注册，控制层就能够与网格的其他成员共享如何与应用程序通信，并强制执行谁可以相互通信的规则。

控制层负责保护网格、促进服务发现、健康检查、策略实施和其他类似的操作问题。

数据层处理服务之间的通信。许多服务网格解决方案使用 sidecar 代理来处理数据层通信，因此限制了服务对网络环境所需的感知水平。

![Overview of a service mesh](https://mktg-content-api-hashicorp.vercel.app/api/assets?product=consul&version=refs%2Fheads%2Fstable-website&asset=website%2Fpublic%2F%2Fimg%2Fwhat_is_service_mesh_1.png)

#### 4、API 网关与服务网格

API 网关是一个集中式访问点，用于处理传入的客户端请求并将其交付给服务。API 网关充当控制层，允许操作员和开发人员管理传入的客户端请求并根据请求应用不同的处理逻辑。API 网关会将传入的请求路由到相应的服务。API 网关的主要功能是处理请求并将服务的回复返回给客户端。

服务网格专门从事服务的网络管理和服务之间的通信。网格负责跟踪服务及其健康状态、IP 地址和流量路由，并确保服务之间的所有流量都经过身份验证和加密。与 API 网关不同，服务网格将跟踪所有已注册服务的生命周期，并确保将请求路由到服务的健康实例。API 网关经常与负载均衡器一起部署，以确保将流量定向到健康且可用的服务实例。网格减少了负载平衡器的占用空间，因为路由职责以分散的方式处理。

API 网关可以与服务网格一起使用，以通过服务网格桥接外部网络（非网格）。

**API 网关和流量方向**：API 网关通常用于接受南北向流量。南北流量是进入或退出数据中心或虚拟专用网络 (VPC) 的网络流量。服务网格主要用于处理东西向流量。东西向流量传统上保留在数据中心或 VPC 内。一个服务网格可以连接到另一个数据中心或 VPC 中的另一个服务网格，以形成一个联邦网格。

#### 5、服务网格解决了哪些问题？

现代基础设施正在从本质上是静态的转变为动态的（短暂的）。这种动态基础设施的生命周期很短，这意味着虚拟机 (VM) 和容器经常被回收。组织很难管理和跟踪依赖于短期资源的应用程序服务。服务网格通过充当所有注册服务的中央注册表来解决这个问题。随着服务实例（例如，VM、容器、无服务器功能）的启动和关闭，网格会知道它们的状态和可用性。进行*服务发现*的能力是服务网格解决其他问题的基础。

由于服务网格了解服务及其实例的状态，因此网格可以实现更智能和动态的网络路由。许多服务网格提供 L7 流量管理功能。因此，运营商和开发人员可以根据需要创建强大的规则来引导网络流量，例如负载平衡、流量拆分、动态故障转移和自定义解析器。服务网格的动态网络行为允许应用程序所有者在不更改应用程序的情况下提高应用程序的弹性和可用性。

随着越来越多的应用程序部署在不同的云提供商（多云）和私有数据中心，实施动态网络行为至关重要。组织可能需要将网络流量路由到其他基础设施环境。确保此流量安全是所有组织的首要任务。服务网格提供了在所有服务之间强制执行网络流量加密 (mTLS) 和身份验证的能力。服务网格可以为每个服务及其实例自动生成一个 SSL 证书。该证书与网格内的其他服务进行身份验证，并使用 SSL 加密 TCP/UDP/gRPC 连接。

规定允许哪些服务相互通信的细粒度策略是服务网格的另一个好处。传统上，允许服务通过防火墙规则与其他服务通信。传统的防火墙（基于 IP）模型难以使用生命周期短且频繁回收 IP 地址的动态基础架构资源实施。结果，网络管理员必须开放网络范围以允许服务之间的网络流量，而不区分产生网络流量的服务。但是，服务网格允许运营商和开发人员从基于 IP 的模型转变为更多地关注服务到服务的权限。运营商定义了只允许*服务 A*与*服务 B通信的策略*. 否则，默认操作是拒绝流量。这种从基于 IP 地址的安全模型到以服务为中心的模型的转变减少了保护网络流量的开销，并允许组织利用多云环境而不会因复杂性而牺牲安全性。

#### 6、如何实现服务网格？

服务网格通常安装在 Kubernetes 集群中。还有一些与平台无关的服务网格可用于非基于 Kubernetes 的工作负载。对于 Kubernetes，大多数服务网格可以由运营商通过[Helm 图表](https://helm.sh/)安装。此外，服务网格可能会提供支持服务网格安装和维护的 CLI 工具。非基于 Kubernetes 的服务网格可以通过基础架构即代码 (IaC) 产品安装，例如[Terraform](https://www.terraform.io/)、CloudFormation、ARM 模板、Puppet、Chef 等。

#### 7、什么是多平台服务网格？

多平台服务网格能够支持各种基础设施环境。这可以从让服务网格支持 Kubernetes 和非 Kubernetes 工作负载，到让服务网格跨越各种云环境（多云和混合云）。

#### 8、什么是consul

Consul 是一种多网络工具，提供功能齐全的服务网格解决方案，可解决运营微服务和云基础设施（多云和混合云）的网络和安全挑战。Consul 提供了一种软件驱动的路由和分段方法。它还带来了额外的好处，例如故障处理、重试和网络可观察性。这些功能中的每一个都可以根据需要单独使用，也可以一起使用以构建完整的服务网格并实现[零信任](https://www.hashicorp.com/solutions/zero-trust-security)安全。简单来说，Consul 就是服务网格的控制层。[Consul 通过其对Envoy](https://www.envoyproxy.io/)作为代理的一流支持来支持数据层。

您可以将 Consul 与虚拟机 (VM)、容器或容器编排平台（例如[Nomad](https://www.nomadproject.io/)和 Kubernetes）一起使用。Consul 与平台无关，因此非常适合所有环境，包括遗留平台。

Consul 可作为[自安装](https://www.consul.io/downloads)项目或称为[HCP Consul](https://portal.cloud.hashicorp.com/sign-in?utm_source=consul_docs)的完全托管的服务网格解决方案提供。HCP Consul 使用户能够发现并安全地连接服务，而不会增加自己维护服务网格的操作负担。

### consul和其它软件

#### 1、概述

Consul 解决的问题是多种多样的，但每个单独的功能都由许多不同的系统解决。虽然没有一个系统可以提供 Consul 的所有功能，但还有其他选项可以解决其中一些问题。

在本节中，我们将 Consul 与其他一些选项进行比较。在大多数情况下，Consul 不会与任何其他系统相互排斥。

#### 2、Chef,Puppet,etc.

使用 Chef、Puppet 和其他配置管理工具来构建服务发现机制的人并不少见。这通常通过查询全局状态以在周期性收敛运行期间在每个节点上构建配置文件来完成。

不幸的是，这种方法有许多缺陷。配置信息是静态的，不能比收敛运行更频繁地更新。通常这是在几分钟或几小时的时间间隔内。此外，没有将系统状态合并到配置中的机制：不健康的节点可能会收到进一步加剧流量的问题。使用这种方法也使支持多个数据中心变得具有挑战性，因为中央服务器组必须管理所有数据中心。

Consul 专门设计为服务发现工具。因此，它更加动态并且对集群的状态做出响应。节点可以注册和注销它们提供的服务，使依赖的应用程序和服务能够快速发现所有提供者。通过使用集成的健康检查，Consul 可以将流量从不健康的节点路由出去，从而使系统和服务能够正常恢复。可以将配置管理工具提供的静态配置移动到动态键/值存储中。这允许在没有缓慢收敛运行的情况下更新应用程序配置。最后，由于每个数据中心独立运行，支持多个数据中心与单个数据中心没有什么不同。

也就是说，Consul 不是配置管理工具的替代品。这些工具对于设置应用程序仍然至关重要，包括 Consul 本身。静态配置最好由现有工具管理，而动态状态和发现最好由 Consul 管理。配置管理和集群管理的分离还有许多有利的副作用：Chef recipes 和 Puppet manifests 在没有全局状态的情况下变得更简单，服务或配置更改不再需要定期运行，并且由于配置管理运行，基础设施可以变得不可变不需要全局状态。

#### 3、Nagios

Nagios 是为监控而构建的工具。它用于在出现问题时快速通知操作员。

Nagios 使用一组中央服务器，这些服务器被配置为对远程主机执行检查。这种设计使得 Nagios 难以扩展，因为大型机队很快就会达到垂直扩展的极限，而 Nagios 不容易水平扩展。Nagios 也很难与现代 DevOps 和配置管理工具一起使用，因为在添加或删除远程服务器时必须更新本地配置。

Consul 提供与 Nagios 相同的健康检查能力，对现代 DevOps 友好，并避免了固有的扩展问题。Consul 在本地运行所有检查，避免给中央服务器带来负担。检查的状态由 Consul 服务器维护，它们具有容错性并且没有单点故障。最后，Consul 可以扩展到更多的检查，因为它依赖于边缘触发的更新。这意味着仅当检查从“通过”转换为“失败”时才会触发更新，反之亦然。

在大型机队中，大多数检查都通过了，即使是少数未通过的检查也是持久的。通过仅捕获更改，Consul 减少了运行状况检查使用的网络和计算资源量，从而使系统更具可扩展性。

精明的读者可能会注意到，如果 Consul 代理死亡，则不会发生边缘触发更新。从其他节点的角度来看，所有检查都将处于稳定状态。但是，Consul 也对此有所防范。客户端和服务器之间使用的 [gossip 协议集成了分布式故障检测器。](https://www.consul.io/docs/architecture/gossip)这意味着如果 Consul 代理失败，将检测到失败，因此该节点运行的所有检查都可以假定失败。这个故障检测器在整个集群之间分配工作，最重要的是，使边缘触发架构能够工作。

#### 4、SkyDNS

SkyDNS 是一个旨在提供服务发现的工具。它使用多个强一致性和容错的中央服务器。节点使用 HTTP API 注册服务，并且可以通过 HTTP 或 DNS 进行查询以执行发现。

Consul 非常相似，但提供了超集的功能。Consul 还依赖多个中央服务器来提供强大的一致性和容错性。节点可以使用 HTTP API 或使用代理来注册服务，并通过 HTTP 或 DNS 进行查询。

然而，这些系统在许多方面有所不同。Consul 提供了更丰富的健康检查框架，支持任意检查和高度可扩展的故障检测方案。SkyDNS 依赖于简单的心跳和 TTL，这是一种已知可扩展性问题的方法。此外，与 Consul 执行的丰富的健康检查相比，心跳仅提供有限的活动检查。

在 SkyDNS 中使用“区域”可以支持多个数据中心；但是，数据是从单个集群中管理和查询的。如果服务器在数据中心之间拆分，复制协议将受到非常长的提交时间的影响。如果所有 SkyDNS 服务器都位于中央数据中心，则连接问题可能会导致整个数据中心失去可用性。此外，即使没有连接问题，查询性能也会受到影响，因为请求必须始终在远程数据中心执行。

Consul 开箱即用地支持多个数据中心，并且它有意将托管数据范围限定为每个数据中心。这意味着每个数据中心都运行一个独立的服务器集群。如有必要，请求被转发到远程数据中心；数据中心内的服务请求永远不会通过 WAN，并且数据中心之间的连接问题不会影响数据中心内的可用性。此外，一个数据中心的不可用性不会影响在任何其他数据中心中发现服务。

#### 5、SmartStack

SmartStack 是一个解决服务发现问题的工具。它有一个相当独特的架构，有 4 个主要组件：ZooKeeper、HAProxy、Synapse 和 Nerve。ZooKeeper 服务器负责以一致且容错的方式存储集群状态。SmartStack 集群中的每个节点都运行 Nerves 和 Synapses。Nerve 负责对服务运行健康检查并在 ZooKeeper 服务器上注册。Synapse 向 ZooKeeper 查询服务提供者并动态配置 HAProxy。最后，客户端与 HAProxy 对话，HAProxy 会在服务提供商之间进行健康检查和负载平衡。

Consul 是一个更简单、更具包容性的系统，因为它不依赖任何外部组件。Consul 使用集成的[gossip 协议](https://www.consul.io/docs/architecture/gossip) 来跟踪所有节点并执行服务器发现。这意味着与 SmartStack 不同，这意味着服务器地址不需要在整个队列范围内进行硬编码和更新。

Consul 和 Nerves 的服务注册可以通过配置文件完成，但 Consul 还支持 API 来动态更改正在使用的服务和检查。

对于发现，SmartStack 客户端必须使用 HAProxy，需要提前为 Synapse 配置所有所需的端点。Consul 客户端使用 DNS 或 HTTP API，无需事先进行任何配置。Consul 还提供“标签”抽象，允许服务提供元数据，例如版本、主要/次要名称或可用于过滤的不透明标签。然后，客户端只能请求具有匹配标签的服务提供者。

这些系统在管理健康检查的方式上也有所不同。Nerve 以类似于 Consul 代理的方式执行本地健康检查。但是，Consul 维护着单独的目录和卫生系统。该划分允许操作员查看每个服务池中有哪些节点，并提供对失败检查的洞察力。Nerve 只是在检查失败时取消注册节点，提供有限的操作洞察力。Synapse 还配置 HAProxy 以执行额外的健康检查。这会导致所有潜在的服务客户端检查活跃度。对于大型车队，这种 N 对 N 样式的健康检查可能会非常昂贵。

Consul 通常提供更丰富的健康检查系统。Consul 支持 Nagios 风格的插件，可以使用大量的检查目录。Consul 允许进行服务级别和主机级别的检查。甚至还有一个“死人开关”检查，允许应用程序轻松集成自定义健康检查。最后，所有这些都集成到带有 API 的 Health and Catalog 系统中，使操作员能够深入了解更广泛的系统。

除了服务发现和健康检查，Consul 还为配置和多数据中心支持提供了一个集成的键/值存储。虽然可以为多个数据中心配置 SmartStack，但中央 ZooKeeper 集群将严重阻碍容错部署。

#### 6、Serf

[Serf](https://www.serf.io/)是一个节点发现和编排工具，也是迄今为止讨论的唯一一个建立在最终一致的 gossip 模型上且没有集中式服务器的工具。它提供了许多功能，包括组成员资格、故障检测、事件广播和查询机制。但是，Serf 不提供任何高级功能，例如服务发现、健康检查或键/值存储。Consul 是一个完整的系统，提供所有这些功能。

Consul 中使用的内部[gossip 协议](https://www.consul.io/docs/architecture/gossip)实际上由 Serf 库提供支持：Consul 利用成员资格和故障检测功能并在它们的基础上添加服务发现。相比之下，Serf 的发现特性是在节点级别，而 Consul 提供服务和节点级别的抽象。

Serf 提供的健康检查级别非常低，仅指示代理是否处于活动状态。Consul 对此进行了扩展，以提供丰富的健康检查系统，除了任意主机和服务级别检查外，还可以处理活跃度。运行状况检查与中央目录集成，操作员可以轻松查询该目录以深入了解集群。

Serf 提供的成员资格是节点级别的，而 Consul 专注于服务级别抽象，将单个节点映射到多个服务。这可以在 Serf 中使用标签进行模拟，但它受到更多限制并且不提供有用的查询接口。Consul 还使用了强一致的目录，而 Serf 只是最终一致的。

除了服务级别抽象和改进的健康检查之外，Consul 还提供键/值存储和对多个数据中心的支持。Serf 可以跨 WAN 运行，但性能下降。Consul 使用[多个 gossip 池](https://www.consul.io/docs/architecture)，以便在 LAN 上的 Serf 性能可以保留，同时仍然在 WAN 上使用它来连接多个数据中心。

Consul 在使用上固执己见，而 Serf 是一种更灵活和通用的工具。在[CAP](https://en.wikipedia.org/wiki/CAP_theorem)术语中，Consul 使用 CP 架构，倾向于一致性而不是可用性。Serf 是一个 AP 系统，为了可用性而牺牲了一致性。这意味着如果中央服务器无法形成法定人数，Consul 将无法运行，而 Serf 将在几乎所有情况下继续运行。

#### 7、Eureka

Eureka 是一个服务发现工具。该架构主要是客户端/服务器，每个数据中心有一组 Eureka 服务器，通常每个可用区一个。通常，Eureka 的客户端使用嵌入式 SDK 来注册和发现服务。对于未原生集成的客户端，使用 Ribbon 等 Sidecar 通过 Eureka 透明地发现服务。

Eureka 使用尽力而为的复制提供了弱一致的服务视图。当客户端向服务器注册时，该服务器将尝试复制到其他服务器，但不提供任何保证。服务注册的生存时间 (TTL) 很短，需要客户端与服务器进行心跳。不健康的服务或节点将停止心跳，导致它们超时并从注册表中删除。发现请求可以路由到任何服务，由于尽力复制，这些服务可以提供陈旧或丢失的数据。这种简化的模型允许轻松的集群管理和高可扩展性。

Consul 提供了一组超级功能，包括更丰富的健康检查、键/值存储和多数据中心感知。Consul 需要在每个数据中心拥有一组服务器，以及每个客户端上的代理，类似于使用像 Ribbon 这样的 sidecar。Consul 代理允许大多数应用程序不知道 Consul，通过配置文件执行服务注册并通过 DNS 或负载平衡器边车进行发现。

Consul 提供了强大的一致性保证，因为服务器使用 [Raft 协议](https://www.consul.io/docs/architecture/consensus)复制状态。Consul 支持一组丰富的健康检查，包括 TCP、HTTP、Nagios/Sensu 兼容脚本或基于 TTL 的 Eureka。客户端节点参与[基于 gossip 的健康检查](https://www.consul.io/docs/architecture/gossip)，它分发健康检查的工作，这与中心化心跳不同，后者成为可扩展性挑战。Discovery requests are routed to the elected Consul leader which allows them to be strongly consistent by default. 允许过时读取的客户端使任何服务器都可以处理他们的请求，从而实现像 Eureka 这样的线性可扩展性。

Consul 的强一致性意味着它可以用作领导选举和集群协调的锁定服务。Eureka 不提供类似的保证，通常需要为需要执行协调或具有更强一致性需求的服务运行 ZooKeeper。

Consul 提供了支持面向服务架构所需的功能工具包。这包括服务发现，还包括丰富的健康检查、锁定、键/值、多数据中心联合、事件系统和 ACL。Consul 和 consul-template 和 envconsul 等工具生态系统都试图尽量减少集成所需的应用程序更改，以避免需要通过 SDK 进行本地集成。Eureka 是更大的 Netflix OSS 套件的一部分，该套件期望应用程序相对同质且紧密集成。因此，Eureka 只解决了有限的问题子集，期望与 ZooKeeper 等其他工具一起使用。

#### 8、Istio

Istio 是一个用于连接、管理和保护微服务的开放平台。

要启用 Istio 的全部功能，必须部署多个服务。对于控制层：必须部署 Pilot、Mixer 和 Citadel，对于数据层，必须部署 Envoy sidecar。此外，Istio 需要来自 Kubernetes、Consul、Eureka 或其他人的 3rd 方服务目录。最后，Istio 需要一个外部系统来存储状态，通常是 etcd。至少，三个 Istio 专用服务以及至少一个独立的分布式系统（除了 Istio）必须配置为使用 Istio 的全部功能。

Istio 为基于路径的路由、流量整形、负载平衡和遥测提供第 7 层功能。可以针对第 7 层和第 4 层属性配置访问控制策略，以根据服务身份控制访问、路由等。

Consul 是一个提供服务器和客户端功能的单一二进制文件，包括服务目录、配置、TLS 证书、授权等的所有功能。无需安装其他系统即可使用 Consul，尽管 Consul 可选择支持外部系统（例如 Vault）来增强行为。这种架构使 Consul 可以轻松安装在任何平台上，包括直接安装到机器上。

Consul 使用基于代理的模型，集群中的每个节点都运行一个 Consul 客户端。此客户端维护从服务器有效更新的本地缓存。因此，所有安全服务通信 API 都在微秒内响应，不需要任何外部通信。这使我们能够在边缘执行连接，而无需与中央服务器通信。Istio 将请求流向中央 Mixer 服务，并且必须通过 Pilot 推送更新。这大大降低了 Istio 的可扩展性，而 Consul 能够有效地分发更新并在边缘执行所有工作。

Consul 为基于路径的路由、流量转移、负载平衡和遥测提供第 7 层功能。Consul 仅对第 4 层强制执行授权和身份验证——要么可以建立 TLS 连接，要么不能。我们认为服务身份应该绑定到第 4 层，而第 7 层应该用于路由、遥测等。未来我们将向 Consul 添加更多第 7 层功能。

Consul 的数据层是可插拔的。它包括一个内置代理，具有更大的性能折衷以方便使用。但是您也可以使用 Envoy 等第三方代理来利用第 7 层功能。为作业使用正确代理的能力允许灵活的异构部署，其中不同的代理可能更适合它们代理的应用程序。我们鼓励用户利用可插拔数据层并使用支持集群所需的第 7 层功能的代理。

除了第三方代理支持外，应用程序还可以与 Connect 协议原生集成。因此，引入 Connect 的性能开销可以忽略不计。这些“Connect-native”应用程序可以与任何其他支持 Connect 的服务交互，无论它们是使用代理还是 Connect-native。

Consul 实现了带有轮换支持的自动 TLS 证书管理。叶证书和根证书都可以在大型 Consul 集群中自动轮换，连接中断为零。证书管理系统可通过 Consul 中的代码更改进行插入，并将很快作为外部插件系统公开。这使 Consul 可以使用任何 PKI 解决方案。

因为Consul的服务连接特性“Connect”是内置的，所以继承了Consul的运行稳定性。自 2014 年以来，Consul 一直在为大公司投入生产，并且众所周知，它可以部署在单个集群中多达 50,000 个节点上。

这种比较是基于我们自己对 Istio 的有限使用以及与 Istio 用户的交流。如果您觉得本次比较中有不准确的表述，请点击本页页脚的“编辑本页”，提出修改意见。我们力求技术准确性，并将尽快审查和更新这篇文章的不准确之处。

#### 9、Envoy and Other Proxies

现代服务代理为微服务和云环境提供高级服务路由、身份验证、遥测等。Envoy 是一种流行且功能丰富的代理，通常单独使用。Consul[与 Envoy 集成](https://www.consul.io/docs/connect/proxies/envoy)以简化其配置。

代理需要一组丰富的配置才能运行，因为后端地址、前端侦听器、路由、过滤器、遥测传送等都必须进行配置。此外，现代基础设施包含许多代理，通常每个服务一个代理，因为代理部署在服务旁边的“边车”模型中。因此，代理的主要挑战是配置蔓延和编排。

代理形成所谓的“数据平面”：数据为网络连接传输的路径。上面是“控制平面”，它为数据平面提供规则和配置。代理通常与外部解决方案集成以提供控制平面。例如，Envoy 与 Consul 集成以动态填充服务后端地址。

Consul 是一个控制平面解决方案。服务目录用作服务及其地址的注册表，可用于为代理路由流量。Consul 的 Connect 功能提供了 TLS 证书和服务访问图，但仍然需要在数据路径中存在代理。作为控制平面，Consul 集成了许多数据平面解决方案，包括 Envoy、HAProxy、Nginx 等。

[Consul Envoy 集成](https://www.consul.io/docs/connect/proxies/envoy) 目前是利用 Consul 提供的高级第 7 层功能的主要方式。除了 Envoy，Consul 还允许第三方代理与 Connect 集成，并为数据平面提供 Consul 作为控制平面运行。

代理为第 7 层问题提供了出色的解决方案，例如基于路径的路由、跟踪和遥测等。通过支持可插拔数据平面模型，可以根据需要部署正确的代理。对于性能关键型应用程序或那些利用第 7 层功能的应用程序，可以使用 Envoy。对于非性能关键的第 4 层应用程序，您可以使用 Consul 的[内置代理](https://www.consul.io/docs/connect/proxies/built-in)来方便。

对于某些可能需要硬件的应用程序，可以部署硬件负载平衡器，例如 F5 设备。Consul 鼓励在场景中使用正确的代理，并将硬件负载均衡器视为可以与其他代理一起运行的可交换组件，假设它们与 Connect 的[必要 API](https://www.consul.io/docs/connect/proxies/integrate)集成 。

#### 10、定制解决方案

随着代码库的增长，单体应用程序通常会演变为面向服务的架构 (SOA)。SOA 的一个普遍痛点是服务发现和配置。在许多情况下，这会导致组织构建本土解决方案。分布式系统很难，这是不争的事实。构建一个容易出错且耗时。大多数系统通过引入单点故障（例如单个 Redis 或 RDBMS）来偷工减料来维护集群状态。这些解决方案可能在短期内有效，但它们很少具有容错性或可扩展性。除了这些限制之外，它们还需要时间和资源来构建和维护。

Consul 提供了开箱即用的 SOA 所需的核心功能集。通过使用 Consul，组织可以利用开源工作来减少重新发明轮子所花费的时间和精力，而可以专注于他们的业务应用程序。

Consul 建立在被广泛引用的研究之上，并且在设计时考虑了分布式系统的限制。在每一步，Consul 都努力为任何规模的组织提供强大且可扩展的解决方案。

## 二、开始使用

### 概述

#### 1、安装consul

安装 Consul 很简单。有三种安装 Consul 的方法：

1. 使用[预编译的二进制文件](https://www.consul.io/docs/install#precompiled-binaries)
2. [从源](https://www.consul.io/docs/install#compiling-from-source)安装
3. [在 Kubernetes 中](https://www.consul.io/docs/k8s/installation/install)安装

下载预编译的二进制文件是最简单的，我们通过 TLS 提供下载以及 SHA256 和来验证二进制文件。我们还分发带有可以验证的 SHA256 和的 PGP 签名。

[入门指南](https://learn.hashicorp.com/tutorials/consul/get-started-install?utm_source=consul.io&utm_medium=docs)提供了在本地计算机上安装和使用 Consul的快速演练。

#### 2、预编译的二进制文件

要安装预编译的二进制文件，请为您的系统[下载](https://www.consul.io/downloads)适当的软件包。Consul 当前打包为 zip 文件。我们没有任何近期提供系统包的计划。

下载压缩包后，将其解压缩到任意目录。里面的`consul`二进制文件是运行 Consul（或`consul.exe`Windows）所必需的。运行 Consul 不需要额外的文件。

将二进制文件复制到系统上的任何位置。如果您打算从命令行访问它，请确保将其放在您的`PATH`.

#### 3、从源代码编译

要从源代码编译，您需要安装[Go](https://golang.org/)并克隆Git仓库在您的Path中

```shell
$ git clone https://github.com/hashicorp/consul.git
$ cd consul
```

为您的目标系统构建 Consul。二进制文件将被放入`./bin` （相对于 git checkout）。

#### 4、验证安装

要验证 Consul 是否正确安装，`consul version`请在您的系统上运行。您应该看到帮助输出。如果您是从命令行执行它，请确保它在您的 PATH 上，否则您可能会收到关于找不到 Consul 的错误。

```shell
$ consul version
```

### consul 代理

本主题概述了 Consul 代理，它是 Consul 的核心流程。代理维护成员信息、注册服务、运行检查、响应查询等。代理必须在属于 Consul 集群的每个节点上运行。

代理以客户端或服务器模式运行。客户端节点是构成集群大部分的轻量级进程。它们与服务器节点交互以进行大多数操作，并保持非常少的自身状态。客户端在运行服务的每个节点上运行。

除了核心代理操作之外，服务器节点还参与[共识仲裁](https://www.consul.io/docs/architecture/consensus)。quorum 基于 Raft 协议，在失败的情况下提供强一致性和可用性。服务器节点应该在专用实例上运行，因为它们比客户端节点更占用资源。

#### 1、生命周期

Consul 集群中的每个代理都会经历一个生命周期。了解生命周期对于构建代理与集群的交互以及集群如何处理节点的心理模型很有用。以下过程描述了现有集群上下文中的代理生命周期：

1. **代理**可以手动启动，也可以通过自动化或程序化过程启动。新启动的代理不知道集群中的其他节点。
2. **代理加入集群**，使代理能够发现代理对等点。[`join`](https://www.consul.io/commands/join)代理在发出命令时或根据[自动加入配置](https://www.consul.io/docs/install/cloud-auto-join)启动时加入集群。
3. **关于代理的信息被传递到整个集群**。结果，所有节点最终都会相互了解。
4. 如果代理是服务器，**现有服务器将开始复制到新节点。**

#### 2、失败和崩溃

如果发生网络故障，一些节点可能无法到达其他节点。无法访问的节点将被标记为*failed*。

区分网络故障和代理崩溃是不可能的。因此，代理崩溃的处理方式与网络故障的处理方式相同。

一旦节点被标记为失败，此信息将在服务目录中更新。

```
注意：仅当服务器仍可形成 quorum时，才可能更新目录。一旦网络恢复或崩溃的代理重新启动，集群将自行修复并将节点取消标记为失败。目录中的健康检查也将更新以反映当前状态。
```

#### 3、退出节点

当一个节点离开集群时，它会传达其意图，并且集群会将节点标记为已*离开*。与与故障相关的更改相比，节点提供的所有服务都会立即取消注册。如果服务器代理离开，复制到现有服务器将停止。

为了防止死节点（处于*失败*或*离开* 状态的节点）的积累，Consul 会自动从目录中删除死节点。这个过程称为*收割(reaping)*。目前这是在可配置的 72 小时间隔上完成的（*不*建议更改收割间隔，因为它在中断情况下会产生后果）。Reaping 类似于离开，导致所有关联的服务被注销。

#### 4、要求

您应该为每台服务器或主机运行一个 Consul 代理。Consul 的实例可以在单独的虚拟机中运行，也可以作为单独的容器运行。每个 Consul 部署至少需要一个服务器代理，但建议使用三到五个服务器代理。有关主机、端口、内存和其他要求的信息，请参阅以下部分：

- [服务器性能](https://www.consul.io/docs/install/performance)
- [所需端口](https://www.consul.io/docs/install/ports)

数据中心[部署教程](https://learn.hashicorp.com/tutorials/consul/reference-architecture?in=consul/production-deploy#deployment-system-requirements)包含其他信息，包括许可配置、环境变量和其他详细信息。

#### 5、启动consul代理

使用以下语法使用`consul`命令和子命令启动 Consul 代理：`agent`

```shell
$ consul agent <options>
```

Consul 附带了一个`-dev`将代理配置为在服务器模式下运行的标志，以及一些使您能够快速开始使用 Consul 的附加设置。该`-dev`标志仅用于学习目的。我们强烈建议不要将其用于生产环境。

```
入门教程：您可以按照 入门教程 测试本地代理。
```

当使用`-dev`标志启动 Consul 时，Consul 需要运行的唯一附加信息是用于存储代理状态数据的目录的位置。您可以使用`-data-dir`标志指定位置或在外部文件中定义位置并使用`-config-file`标志指向文件。

`-config-dir`您还可以使用该标志指向包含多个配置文件的目录。这使您能够在逻辑上将配置设置分组到单独的文件中。有关更多信息，请参阅[配置 Consul 代理](https://www.consul.io/docs/agent#configuring-consul-agents)。

以下示例以开发模式启动代理并将代理状态数据存储在`tmp/consul`目录中：

```shell
$ consul agent -data-dir=tmp/consul -dev
```

代理是高度可配置的，这使您能够将 Consul 部署到任何基础设施。该命令的许多默认选项`agent`都适合熟悉 Consul 的本地实例。然而，在实践中，必须为 Consul 指定几个额外的配置选项才能按预期运行。有关配置选项的完整列表，请参阅[代理配置](https://www.consul.io/docs/agent/options)主题。

#### 6、了解代理启动输出

```shell
$ consul agent -data-dir=/tmp/consul
==> Starting Consul agent...
==> Consul agent running!
       Node name: 'Armons-MacBook-Air'
      Datacenter: 'dc1'
          Server: false (bootstrap: false)
     Client Addr: 127.0.0.1 (HTTP: 8500, DNS: 8600)
    Cluster Addr: 192.168.1.43 (LAN: 8301, WAN: 8302)

==> Log data will now stream in as it occurs:

    [INFO] serf: EventMemberJoin: Armons-MacBook-Air.local 192.168.1.43
...
```

- **Node name 节点名称**：这是代理的唯一名称。默认情况下，这是机器的主机名，但您可以使用 [`-node`](https://www.consul.io/docs/agent/options#_node)标志自定义它。
- **Datacenter 数据中心**：这是配置代理运行的数据中心。对于单 DC 配置，代理将默认为`dc1`，但您可以使用[`-datacenter`](https://www.consul.io/docs/agent/options#_datacenter)标志配置代理向哪个数据中心报告。Consul 对多个数据中心具有一流的支持，但配置每个节点以报告其数据中心可提高代理效率。
- **Server 服务器**：这表明代理是在服务器模式还是客户端模式下运行。在服务器模式下运行代理需要额外的开销。这是因为它们参与了共识仲裁、存储集群状态并处理查询。服务器也可能处于[“引导”](https://www.consul.io/docs/agent/options#_bootstrap_expect)模式，这使服务器能够选举自己作为 Raft 领导者。多个服务器不能处于引导模式，因为它会使集群处于不一致的状态。
- **Client Addr**：这是用于代理的客户端接口的地址。这包括 HTTP 和 DNS 接口的端口。默认情况下，这仅绑定到 localhost。如果更改此地址或端口，则必须`-http-addr`在运行命令时指定 a，例如 [`consul members`](https://www.consul.io/commands/members)指示如何访问代理。其他应用程序也可以使用 HTTP 地址和端口 [来控制 Consul](https://www.consul.io/api)。
- **Cluster Addr**：这是用于集群中 Consul 代理之间通信的地址和端口集。并非集群中的所有 Consul 代理都必须使用相同的端口，但所有其他节点**必须**可以访问此地址。

在 Linux下运行`systemd`时，Consul 通过发送 `READY=1`到`$NOTIFY_SOCKET`LAN 加入完成时通知 systemd。为此，必须设置`join`or`retry_join`选项，并且必须设置服务定义文件`Type=notify`。

#### 7、配置consul代理

您可以指定许多选项来配置 Consul 在发出`consul agent`命令时的操作方式。您还可以创建一个或多个配置文件，并在启动时使用`-config-file`or`-config-dir`选项将它们提供给 Consul。配置文件必须以 JSON 或 HCL 格式编写。

```
Consul 术语：配置文件用于配置客户端代理时，有时称为“服务定义”文件。这是因为客户端最常用于在 Consul 目录中注册服务。
```

以下示例启动一个 Consul 代理，该代理从`server.json`位于当前工作目录中的一个名为的文件中获取配置设置：

```shell
$ consul agent -config-file=server.json
```

成功使用 Consul 所需的配置选项取决于几个因素，包括您正在配置的代理类型（客户端或服务器）、您要部署到的环境类型（例如，本地、多云等），以及您要实施的安全选项（ACL、gRPC 加密）。以下示例旨在帮助您了解可以实现配置 Consul 的一些组合。

#### 8、常用配置设置

配置文件（在向 Consul 注册服务时也称为服务定义文件）中常用以下设置来配置 Consul 代理：

| 范围         | 描述                                                         | 默认                                                         |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| `node_name`  | 指定代理节点名称的字符串值。 详情请参阅[`-node-id`](https://www.consul.io/docs/agent/options#_node_id)。 | 机器的主机名                                                 |
| `server`     | 确定代理是否在服务器模式下运行的布尔值。 详情请参阅[`-server`](https://www.consul.io/docs/agent/options#_server)。 | `false`                                                      |
| `datacenter` | 指定代理在哪个数据中心运行的字符串值。有关详细信息， 请参阅[-datacenter](https://www.consul.io/docs/agent/options#_datacenter)。 | `dc1`                                                        |
| `data_dir`   | 字符串值，指定用于存储代理状态数据的目录。 详情请参阅[`-data-dir`](https://www.consul.io/docs/agent/options#_data_dir)。 | 没有                                                         |
| `log_level`  | 指定记录代理报告的级别的字符串值。 详情请参阅[`-log-level`](https://www.consul.io/docs/docs/agent/options#_log_level)。 | `info`                                                       |
| `retry_join` | 字符串值数组，指定启动后要加入的一个或多个代理地址。代理将继续尝试加入指定的代理，直到成功加入另一个成员。 详情请参阅[`-retry-join`](https://www.consul.io/docs/agent/options#_retry_join)。 | 没有                                                         |
| `addresses`  | 嵌套对象块，定义绑定到代理以进行内部集群通信的地址。         | `"http": "0.0.0.0"`[有关默认地址值](https://www.consul.io/docs/agent/options#addresses)，请参阅代理配置页面 |
| `ports`      | 定义绑定到代理地址的端口的嵌套对象块。 有关详细信息，请参阅（链接到地址选项）。 | [有关默认端口值](https://www.consul.io/docs/agent/options#ports)，请参阅代理配置页面 |

#### 9、服务网格中的服务器节点

以下示例配置适用于名为“ `consul-server`”的服务器代理。服务器已[启动](https://www.consul.io/docs/agent/options#_bootstrap)并启用了 Consul GUI。为服务网格配置此服务器代理的原因`connect`是启用了配置。Connect 是 Consul 的服务网格组件，它使用相互传输层安全性 (TLS) 提供服务到服务的连接授权和加密。应用程序可以在服务网格配置中使用 sidecar 代理来为入站和出站连接建立 TLS 连接，而完全不知道 Connect。有关详细信息，请参阅[连接](https://www.consul.io/docs/connect)。

```json
{
  "node_name": "consul-server",
  "server": true,
  "bootstrap": true,
  "ui_config": {
    "enabled": true
  },
  "datacenter": "dc1",
  "data_dir": "consul/data",
  "log_level": "INFO",
  "addresses": {
    "http": "0.0.0.0"
  },
  "connect": {
    "enabled": true
  }
}
```

#### 10、启用加密的服务器节点

以下示例显示了配置为启用加密的服务器节点。有关如何为 Consul 配置安全选项的更多信息，请参阅[安全章节。](https://www.consul.io/docs/security)

```json
{
  "node_name": "consul-server",
  "server": true,
  "ui_config": {
    "enabled": true
  },
  "data_dir": "consul/data",
  "addresses": {
    "http": "0.0.0.0"
  },
  "retry_join": ["consul-server1", "consul-server2"],
  "encrypt": "aPuGh+5UDskRAbkLaXRzFoSOcSM+5vAK+NEYOWHJH7w=",
  "verify_incoming": true,
  "verify_outgoing": true,
  "verify_server_hostname": true,
  "ca_file": "/consul/config/certs/consul-agent-ca.pem",
  "cert_file": "/consul/config/certs/dc1-server-consul-0.pem",
  "key_file": "/consul/config/certs/dc1-server-consul-0-key.pem"
}
```

#### 11、客户端节点注册服务

使用 Consul 作为中央服务注册中心是一个常见的用例。以下示例配置包括向 Consul 代理注册服务和启用健康检查的常用设置（请参阅[检查](https://www.consul.io/docs/discovery/checks)以了解有关健康检查的更多信息）：

```json
{
  "node_name": "consul-client",
  "server": false,
  "datacenter": "dc1",
  "data_dir": "consul/data",
  "log_level": "INFO",
  "retry_join": ["consul-server"],
  "service": {
    "id": "dns",
    "name": "dns",
    "tags": ["primary"],
    "address": "localhost",
    "port": 8600,
    "check": {
      "id": "dns",
      "name": "Consul DNS TCP on port 8600",
      "tcp": "localhost:8600",
      "interval": "10s",
      "timeout": "1s"
    }
  }
}
```

#### 12、具有多个接口或 IP 地址的客户端节点

以下示例显示了如何使用[go-sockaddr 模板](https://godoc.org/github.com/hashicorp/go-sockaddr/template)配置 Consul 以侦听多个接口或 IP 地址。

`bind_addr`用于内部 RPC 和 Serf 通信（阅读[代理配置了解更多信息](https://www.consul.io/docs/agent/options#bind_addr)）。

该`client_addr`配置指定用于 HTTP、HTTPS、DNS 和 gRPC 服务器的 IP 地址。（[阅读代理配置了解更多信息](https://www.consul.io/docs/agent/options#client_addr)）。

```json
{
  "node_name": "consul-server",
  "server": true,
  "bootstrap": true,
  "ui_config": {
    "enabled": true
  },
  "datacenter": "dc1",
  "data_dir": "consul/data",
  "log_level": "INFO",
  "bind_addr": "{{ GetPrivateIP }}",
  "client_addr": "{{ GetPrivateInterfaces | exclude \"type\" \"ipv6\" | join \"address\" \" \" }} {{ GetAllInterfaces | include \"flags\" \"loopback\" | join \"address\" \" \" }}",
  "advertise_addr": "{{ GetInterfaceIP \"en0\"}}"
}
```

#### 13、停止代理

可以通过两种方式停止代理：优雅或强制。服务器和客户端的行为都不同，具体取决于执行的休假。发送系统信号后，进程可能处于两种潜在状态： *left*和*failed*。

要优雅地停止代理，请向进程发送*中断信号*（通常 `Ctrl-C`来自终端或正在运行的`kill -INT consul_pid`）。有关`kill`命令发送的不同信号的更多信息，请参见 [此处](https://www.linux.org/threads/kill-signals-and-commands-revised.11625/)

当客户端正常退出时，代理首先通知集群它打算离开集群。这样，其他集群成员会通知集群该节点已*离开*。

当服务器正常退出时，服务器不会被标记为*left*。这是为了尽量减少对共识法定人数的影响。相反，服务器将被标记为*failed*。要从集群中删除服务器，请 [`force-leave`](https://www.consul.io/commands/force-leave)使用该命令。只要服务器代理不活动，使用 `force-leave`就会将服务器实例置于*左状态。*

或者，您可以通过向其发送 `kill -KILL consul_pid`信号来强制停止代理。这将立即停止任何代理。集群的其余部分最终会（通常在几秒钟内）检测到节点已经死亡，并通知集群节点已经*失败*。

*对于客户端代理，节点故障*和节点*离开*之间的区别 对于您的用例可能并不重要。例如，对于 Web 服务器和负载均衡器设置，两者都会产生相同的结果：从负载均衡器池中删除 Web 节点。

和配置选项允许您调整此行为[`skip_leave_on_interrupt`](https://www.consul.io/docs/agent/options#skip_leave_on_interrupt)。 [`leave_on_terminate`](https://www.consul.io/docs/agent/options#leave_on_terminate)
